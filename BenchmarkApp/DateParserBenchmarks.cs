﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenchmarkApp
{
    [MemoryDiagnoser]
    [Orderer(BenchmarkDotNet.Order.SummaryOrderPolicy.FastestToSlowest)]
    [RankColumn]
    public class DateParserBenchmarks
    {
        private static readonly DateParser Parser = new DateParser();

        [Params("2019-12", "2019-12-13T16:33:06Z")]
        public string DateTime;

        [Benchmark]
        public void GetYearFromDateTime()
        {
            Parser.GetYearFromDateTime(DateTime);
        }

        [Benchmark]
        public void GetYearFromSplit()
        {
            Parser.GetYearFromSplit(DateTime);
        }

        [Benchmark]
        public void GetYearFromSubstring1()
        {
            Parser.GetYearFromSubstring1(DateTime);
        }

        [Benchmark]
        public void GetYearFromSubstring2()
        {
            Parser.GetYearFromSubstring2(DateTime);
        }

        [Benchmark]
        public void GetYearFromSpan()
        {
            Parser.GetYearFromSpan(DateTime);
        }

        [Benchmark]
        public void GetYearFromManualConversion()
        {
            Parser.GetYearFromMangualConversion(DateTime);
        }

        [Benchmark]
        public void GetYearFromSpanManulConversion()
        {
            Parser.GetYearFromSpanManulConversion(DateTime);
        }
    }
}
