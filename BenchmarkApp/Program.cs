﻿using BenchmarkDotNet.Running;

namespace BenchmarkApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<DateParserBenchmarks>();
        }
    }
}