﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenchmarkApp
{
    public class DateParser
    {
        public int GetYearFromDateTime(string dateTimeAsString)
        {
            var dateTime = DateTime.Parse(dateTimeAsString);
            return dateTime.Year;
        }

        public int GetYearFromSplit(string dateTimeAsString)
        {
            var dateTimeSplit = dateTimeAsString.Split('-');
            return int.Parse(dateTimeSplit[0]);
        }

        public int GetYearFromSubstring1(string dateTimeAsString)
        {
            var indexOfHyphen = dateTimeAsString.IndexOf('-');
            return int.Parse(dateTimeAsString.Substring(0, indexOfHyphen));
        }

        public int GetYearFromSubstring2(string dateTimeAsString)
        {
            var indexOfHyphen = dateTimeAsString.IndexOf('-');
            return int.Parse(dateTimeAsString[0..indexOfHyphen]);
        }

        public int GetYearFromSpan(ReadOnlySpan<char> dateTimeAsSpan)
        {
            var indexOfHyphen = dateTimeAsSpan.IndexOf('-');
            return int.Parse(dateTimeAsSpan.Slice(0, indexOfHyphen));
        }

        public int GetYearFromMangualConversion(string dateTimeAsString)
        {
            var indexOfHyphen = dateTimeAsString.IndexOf('-');

            var count = 0;
            var temp = 0;

            while (count < indexOfHyphen)
                temp = (temp * 10) + (dateTimeAsString[count++] - '0');

            return temp;
        }

        public int GetYearFromSpanManulConversion(ReadOnlySpan<char> dateTimeAsSpan)
        {
            var indexOfHyphen = dateTimeAsSpan.IndexOf('-');

            var count = 0;
            var temp = 0;

            while (count < indexOfHyphen)
                temp = (temp * 10) + (dateTimeAsSpan[count++] - '0');

            return temp;
        }
    }
}
